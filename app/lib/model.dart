import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;

Future<EventKonten> fetchEvent() async {
  final response = await http.get(
    Uri.parse('https://api.nextgenapp.net/api-nextgen/event/'),
    // Send authorization headers to the backend.
    headers: {
      HttpHeaders.authorizationHeader: '1a2f563a88755bd45fe93f797dc5e179d175db6d ',
    },
  );
  final responseJson = jsonDecode(response.body);

  return EventKonten.fromJson(responseJson);
}

List<EventKonten> parseEventKonten(String responseBody) {
  final parsed = jsonDecode(responseBody).cast<Map<String, dynamic>>();

  return parsed.map<EventKonten>((json) => EventKonten.fromJson(json)).toList();
}

class EventKonten {
  final String name;
  final String image;
  final String description;

  const EventKonten({
    required this.name,
    required this.image,
    required this.description,
  });

  factory EventKonten.fromJson(Map<String, dynamic> json) {
    return EventKonten(
      name: json['name'],
      image: json['image'],
      description: json['description'],
    );
  }
}