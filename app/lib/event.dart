import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'dart:async';
import 'dart:convert';

import 'package:app/model.dart';
import 'package:app/contoh_model.dart';


class Event extends StatefulWidget {
  const Event({Key? key}) : super(key: key);

  

  @override
  State<Event> createState() => _EventState();
}



class _EventState extends State<Event> {

  @override
  void initState(){
    super.initState();
    fetchEvent();
  }
  
  List<Konten> content = allKonten;

  
  
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Event List', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
        backgroundColor: Colors.white,
      ),
      body: Container(
      height: 10000,
      child: ListView.builder(
        scrollDirection: Axis.vertical,
        itemCount: content.length,
        itemBuilder: (BuildContext context, int index){
          return Card(
            shadowColor: Colors.grey.withOpacity(.2),
            elevation: 2,
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
            child: Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.all(10),
              decoration: const BoxDecoration(color: Colors.white, borderRadius: BorderRadius.all(Radius.circular(8))),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Image.network(
                    content[index].urlImage,
                    fit: BoxFit.cover,
                    height: 170,
                  ),

                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        content[index].category,
                        style: const TextStyle(fontWeight: FontWeight.bold, color: Colors.blueAccent, fontSize: 16),
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),

                      Text(
                        content[index].title,
                        style: const TextStyle(fontWeight: FontWeight.bold, color: Colors.black, fontSize: 16),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),

                    ],
                  )
                ],
              )
            )
          );
        }
        )
    )
    );
}}